# mumble-web

Full rework of [mumble-web](https://github.com/johni0702/mumble-web) to implement single click calls

### Installing

#### Download


```
git clone https://bitbucket.org/kerrigan/mumble-web-headless
cd mumble-web-headless
yarn install
yarn build
```

Either way you will end up with a `dist` folder that contains the static page.

#### Setup

Replace `<proxybox>` with the host name of the machine where `mumble-web-proxy` is running. If `mumble-web-proxy` is running on the same machine as your web server, use `localhost`.

* NGINX configuration file
```Nginx
server {
        listen 443 ssl;
        server_name voice.example.com;
        ssl_certificate /etc/letsencrypt/live/voice.example.com/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/voice.example.com/privkey.pem;

        location / {
                root /path/to/dist;
        }
        location /demo {
                proxy_pass http://<proxybox>:64737;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection $connection_upgrade;
        }
}

map $http_upgrade $connection_upgrade {
        default upgrade;
        '' close;
}
```

To run `mumble-web-proxy`, execute the following command. Replace `<mumbleserver>` with the host name of your Mumble server (the one you connect to using the normal Mumble client).
Note that even if your Mumble server is running on the same machine as your `mumble-web-proxy`, you should use the external name because (by default, for disabling see its README) `mumble-web-proxy` will try to verify the certificate provided by the Mumble server and fail if it does not match the given host name.
```
mumble-web-proxy --listen-ws 64737 --server <mumbleserver>:64738
```
If your mumble-web-proxy is running behind a NAT or firewall, take note of the respective section in its README.

Make sure that your Mumble server is running. You may now open `https://voice.example.com` in a web browser. You will be prompted for server details: choose either `address: voice.example.com/demo` with `port: 443` or `address: voice.example.com` with `port: 443/demo`. You may prefill these values by appending `?address=voice.example.com/demo&port=443`. Choose a username, and click `Connect`: you should now be able to talk and use the chat.

Here is an example of systemd service, put it in `/etc/systemd/system/mumble-web.service` and adapt it to your needs:
```
[Unit]
Description=Mumble web interface
Documentation=https://github.com/johni0702/mumble-web
Requires=network.target mumble-server.service
After=network.target mumble-server.service

[Service]
Type=simple
User=www-data
ExecStart=/usr/bin/websockify --web=/usr/lib/node_modules/mumble-web/dist --ssl-target localhost:64737 localhost:64738

[Install]
WantedBy=multi-user.target
```

Then
```
systemctl daemon-reload
systemctl start mumble-web
systemctl enable mumble-web
```

### License
ISC

[Mumble]: https://wiki.mumble.info/wiki/Main_Page
[mumble-web-proxy]: https://github.com/johni0702/mumble-web-proxy

