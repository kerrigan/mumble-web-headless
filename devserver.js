var express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');
const https = require("https");
const fs = require('fs');

// proxy middleware options
var options = {
        target: 'http://localhost:1234', // target host
        changeOrigin: true,               // needed for virtual hosted sites
        ws: true                         // proxy websockets
    };

// create the proxy (without context)
var staticProxy = createProxyMiddleware(options);


var apiOpts = {
  changeOrigin: true,               // needed for virtual hosted sites
  ws: true                         // proxy websockets
}


var apiProxy = createProxyMiddleware('http://localhost:3001', apiOpts);
//var wsProxy = proxy('ws://localhost:3000', apiOpts);
// mount `exampleProxy` in web server



var app = express();
    //app.use(wsProxy);
    app.use('/api/', apiProxy);
    //app.use('/socket.io', apiProxy);
    //app.use('/socket.io', wsProxy);
    app.use('/', staticProxy);


const secureServer = https.createServer({
      key: fs.readFileSync('./server.key'),
      cert: fs.readFileSync('./server.cert')
      }, app);

//var server = app.listen(3002);
secureServer.listen(3002);
