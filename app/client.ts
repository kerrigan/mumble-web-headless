import * as log from 'loglevel';
import audioContext from 'audio-context'
import { createAudioMeter } from "./volume-meter"


import mumbleConnect from 'mumble-client-websocket'
import MumbleClient from 'mumble-client/src/client'

import { VADVoiceHandler, VoiceHandler } from './voice'


interface Connector {
    connect(address, options)
}

interface Outmeters {
    [Key: string]: ScriptProcessorNode;
}

export class VoiceClient {
    // Mumble
    private connector: Connector
    client: MumbleClient = null
    private username: string


    // Audio
    private ctx: AudioContext
    private _delayedMicNode: MediaStreamAudioDestinationNode
    private _micNode: MediaStreamAudioSourceNode
    _micStream: MediaStream
    private _delayNode: DelayNode

    // audio info
    meter: ScriptProcessorNode
    outmeters: Outmeters = {}

    // voice handler
    voiceHandler: VoiceHandler = null
    testVoiceHandler: VoiceHandler = null


    // callbacks
    private disconnectedCallback: () => void = null;

    constructor() {
        this.connector = { connect: mumbleConnect }
    }

    connect(
        username: string,
        host: string,
        port: number,
        tokens: Array<string> = [],
        password: string,
        channelName: string = "",
        connectedCallback: () => void = null,
        disconnectedCallback: () => void = null,
        iceServers: Array<RTCIceServer> = []
        ) {
        this.resetClient()
        this.initSound()

        this.username = username;
        this.disconnectedCallback = disconnectedCallback;

        log.info('connecting')

        let self = this;

        this.connector.connect(`wss://${host}:${port}`, {
            username: username,
            password: password,
            webrtc: {
                enabled: true,
                required: true,
                mic: this._delayedMicNode.stream,
                audioContext: this.ctx,
                onAddTrack: this.onAddTrack.bind(this),
                iceServers: iceServers
            },
            tokens: tokens
        }).done(client => {
            log.info('connected')

            this.client = client

            // Prepare for connection errors
            client.on('error', (err) => {
                log.info('logentry.connection_error', err)
                this.resetClient()
                if(!!self.disconnectedCallback) {
                    self.disconnectedCallback();
                    self.disconnect();
                }
            })

            // Register all channels, recursively 
            if (channelName.indexOf("/") != 0) {
                channelName = "/" + channelName;
            }

            const registerChannel = (channel, channelPath) => {
                this.newChannel(channel)
                if (channelPath === channelName) {
                    client.self.setChannel(channel)
                }
                channel.children.forEach(ch => registerChannel(ch, channelPath + "/" + ch.name))
            }
            registerChannel(client.root, "")

            // Register all users
            client.users.forEach(user => this.newUser(user))

            // Register future channels
            client.on('newChannel', channel => this.newChannel(channel))

            // Register future users
            client.on('newUser', user => this.newUser(user))


            // Handle messages
            client.on('message', (sender, message, users, channels, trees) => {
                sender = sender || { __ui: 'Server' }
                log.info({
                    type: 'chat-message',
                    channel: channels.length > 0,
                    message: message
                })
            })

            // Log permission denied error messages
            client.on('denied', (type) => {
                log.info({
                    type: 'generic',
                    value: 'Permission denied : ' + type
                })
            })

            client.on('reject', () => {
                log.info("disconnected");
            })


            // Log welcome message
            if (client.welcomeMessage) {
                log.info({
                    type: 'welcome-message',
                    message: client.welcomeMessage
                })
            }


            this.updateVoiceHandler()

            this.meter = createAudioMeter(this.ctx, 0.98, 0.98, 750);
            var mediaStreamSource = this.ctx.createMediaStreamSource(this._micStream);
            mediaStreamSource.connect(this.meter);

            if (connectedCallback != null) {
                connectedCallback()
            }
        })

    }

    disconnect() {
        if (this.client) {
            this.client.disconnect()
        }
        this.client = null

        this._micNode.disconnect()
        this._delayNode.disconnect()
        var tracks = this._micStream.getAudioTracks();
        if (tracks) {
            tracks[0].enabled = false;
        }
    }


    private resetClient() {
        if (this.client != null) {
            this.client.disconnect()
        }
        this.client = null
    }

    private initSound() {
        let ctx = audioContext()
        this.ctx = ctx
        if (!this._delayedMicNode) {
            this._micNode = ctx.createMediaStreamSource(this._micStream)
            this._delayNode = ctx.createDelay()
            this._delayNode.delayTime.value = 0.15
            this._delayedMicNode = ctx.createMediaStreamDestination()
        }
    }

    private newChannel(channel) {
        const simpleProperties = {
            position: 'position',
            name: 'name',
            description: 'description'
        }

        channel.on('update', properties => {

        }).on('remove', () => {

        })
    }

    private newUser(user) {
        const simpleProperties = {
            uniqueId: 'uid',
            username: 'name',
            mute: 'mute',
            deaf: 'deaf',
            suppress: 'suppress',
            selfMute: 'selfMute',
            selfDeaf: 'selfDeaf',
            texture: 'rawTexture',
            textureHash: 'textureHash',
            comment: 'comment'
        }

        let self = this;

        user.on('update', (actor, properties) => {
        }).on('remove', () => {
            this.onRemoveTrack(user.ssrc)
            //console.log(user, 'exited');
            if(user.username == this.username) {
                log.info("Kicked");
                if(!!self.disconnectedCallback) {
                    self.disconnectedCallback();
                    self.disconnect();
                }
            }
        }).on('voice', stream => {
            log.info(`User ${user.username} started talking`)
            let userNode
            if (stream.target === 'normal') {
                //ui.talking('on')
            } else if (stream.target === 'shout') {
                //ui.talking('shout')
            } else if (stream.target === 'whisper') {
                //ui.talking('whisper')
            }
            stream.on('data', data => {
            }).on('end', () => {
                log.info(`User ${user.username} stopped talking`)
                //ui.talking('off')
            })
        })
    }

    private updateVoiceHandler() {
        //TODO: extract to settings
        let audioBitrate = 40000 // bits per second
        let samplesPerPacket = 960

        if (this.client == null) {
            return
        }

        if (this.voiceHandler != null) {
            this.voiceHandler.end()
            this.voiceHandler = null
        }

        let mode = 'vad'
        //TODO: maybe multiple voice handlers
        this.voiceHandler = new VADVoiceHandler(this.client, { vadLevel: 0.3 })

        this.voiceHandler.on('started_talking', () => {
        })

        this.voiceHandler.on('stopped_talking', () => {
        })

        this._micNode.disconnect()
        this._delayNode.disconnect()
        if (mode === 'vad') {
            this._micNode.connect(this._delayNode)
            this._delayNode.connect(this._delayedMicNode)
        } else {
            this._micNode.connect(this._delayedMicNode)
        }

        this.client.setAudioQuality(
            audioBitrate,
            samplesPerPacket
        )
    }

    private onAddTrack(stream: MediaStream, elem: HTMLAudioElement) {
        var ctx = this.ctx;
        var meter = createAudioMeter(ctx, 0.98, 0.98, 750);
        var mediaStreamSource = ctx.createMediaStreamSource(stream);
        mediaStreamSource.connect(meter);
        this.outmeters[stream.id] = meter;
    }

    private onRemoveTrack(ssrc) {
        delete this.outmeters[ssrc]
    }
}