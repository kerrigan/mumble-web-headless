import { VoiceClient } from './client'
import { initVoice } from './voice'
import * as log from 'loglevel'
import 'webrtc-adapter'

import os from 'platform-detect/os.mjs'


import * as Sentry from "@sentry/browser";
import { Integrations } from "@sentry/tracing";
import { CaptureConsole as CaptureConsoleIntegration } from "@sentry/integrations";


if(!!process.env.SENTRY_DSN) {
    Sentry.init({
        release: "mumble-headless@0.0.1",
        dsn: process.env.SENTRY_DSN,
        integrations: [new Integrations.BrowserTracing({
            tracingOrigins: ["localhost", process.env.HOST, /^\//]}
        ), new CaptureConsoleIntegration({})],
    
        // Set tracesSampleRate to 1.0 to capture 100%
        // of transactions for performance monitoring.
        // We recommend adjusting this value in production
        tracesSampleRate: 1.0,
        debug: !!process.env.DEBUG
    });
}


var connecting = false;
var connected = false;

if(process.env.DEBUG) {
    log.setLevel(log.levels.DEBUG);
} else {
    log.setLevel(log.levels.SILENT);
}

if(os.ios === true) {
    alert("iOS не поддерживается");
    window.close();
}

function normalize_nickname(username) {
    return username.replace(/ /g, '_');
}

async function connect() {
    let username = prompt("Как вас называть?");
    if (username == null) {
        return;
    }
    if (username.trim().length == 0) {
        alert("Вы не ввели имя");
        return;
    }

    username = normalize_nickname(username);

    connecting = true;
    setState('connecting')

    let client = new VoiceClient()
    try {
        const userMedia = await initVoice(data => {
            if (client.client == null) {
                if (client.voiceHandler != null) {
                    client.voiceHandler.end()
                }
                client.voiceHandler = null
            } else if (client.voiceHandler != null) {
                client.voiceHandler.write(data)
            }
        })

        client._micStream = userMedia

        try {

            let formData = new URLSearchParams({
                'username': username
            })

            let response = await fetch("/api/new", {
                method: 'POST',
                body: formData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
            if (response.status == 429) {
                alert("Сервер переполнен, попробуйте позже");
                setState('start');
                userMedia.getTracks().forEach((track) => track.stop());
                return;
            }
            let data = await response.json();
            client.connect(data.username, data.server, data.port, [], "", data.channelname,
                () => {
                    connecting = false;
                    connected = true;
                    setState('connected')
                    startLevelsAnimation(client)
                },
                () => {
                    //console.log("Disconnected");
                    setState('disconnected');
                    userMedia.getTracks().forEach((track) => track.stop());
                },
                JSON.parse(process.env.ICE_SERVERS) as Array<RTCIceServer> || []
            )
        } catch (connectError) {
            console.log(connectError)
            setState('disconnected')
        }
    } catch (error) {
        console.log('Failed to initialize user media\nRefresh page to retry.\n' + error)
        alert('Микрофон недоступен')
        setState('disconnected')
        return
    }
}


export function startLevelsAnimation(client) {
    var start = null;
    window.requestAnimationFrame(function drawStep(timestamp) {
        if (!start) {
            start = timestamp;
        }

        setMicLevel(client.meter.volume * 100);


        var outvolume = 0;
        for (const key in client.outmeters) {
            if (Object.hasOwnProperty.call(client.outmeters, key)) {
                const meter = client.outmeters[key];
                outvolume += meter.volume;
            }
        }
        outvolume = outvolume * 100;

        setSpeakerLevel(outvolume);
        window.requestAnimationFrame(drawStep)
    })
}

var item = document.getElementById("bg");
var statusItem = document.getElementById("status");
var indicatorsItem = document.getElementById("indicators");


var micLevelItem = document.getElementById("micmeter");
var speakerLevelItem = document.getElementById("soundmeter");
var callFinishedItem = document.getElementById("disconnected");

function setState(state) {
    // state: start, connecting, connected
    switch (state) {
        case "start":
            statusItem.innerHTML = "Зайти<br> в эфир";
            statusItem.style.visibility = "";
            callFinishedItem.style.visibility = "hidden";
            indicatorsItem.style.visibility = "hidden";
            item.classList.remove('button-bg-connecting');
            break;
        case "connecting":
            statusItem.innerHTML = "Подключение";
            statusItem.style.visibility = "";
            callFinishedItem.style.visibility = "hidden";
            indicatorsItem.style.visibility = "hidden";
            item.classList.add('button-bg-connecting');
            break;
        case "connected":
            statusItem.style.visibility = "hidden";
            callFinishedItem.style.visibility = "hidden";
            indicatorsItem.style.visibility = "";
            item.classList.remove('button-bg-connecting');
            break;
        case "disconnected":
            statusItem.style.visibility = "hidden";
            callFinishedItem.style.visibility = "";
            indicatorsItem.style.visibility = "hidden";
            item.classList.remove('button-bg-connecting');
            break;
    }
}

function setMicLevel(level) {
    var inset = 100 - level;
    micLevelItem.style.clipPath = "inset(" + inset + "% 0 0 0)"
}

function setSpeakerLevel(level) {
    var inset = 100 - level;
    speakerLevelItem.style.clipPath = "inset(" + inset + "% 0 0 0)"
}


/*
document.getElementById("btnstart").addEventListener('click', (e) => {
    setState('start');
})

document.getElementById("btnconnecting").addEventListener('click', (e) => {
    setState('connecting');
})


document.getElementById("btnconnected").addEventListener('click', (e) => {
    setState('connected');
})

document.getElementById("btnconnect").addEventListener('mousedown', (e) => {
    connect()
})
*/

document.getElementById("bg").addEventListener('click', (e) => {
    if (!connecting && !connected) {
        connect()
    }
})
